package uk.lobsterdoodle.namepicker.dependency;

import dagger.Module;
import dagger.Provides;
import uk.lobsterdoodle.namepicker.ScrollingActivity;
import uk.lobsterdoodle.namepicker.app.NamesApplication;
import uk.lobsterdoodle.namepicker.core.KotlinNameGenerator;
import uk.lobsterdoodle.namepicker.core.NameGenerator;
import uk.lobsterdoodle.namepicker.core.NumberGenerator;
import uk.lobsterdoodle.namepicker.core.RandomNumberGenerator;
import uk.lobsterdoodle.namepicker.presenter.DrawActivityPresenter;

@Module(injects = {
        ScrollingActivity.class,
        NamesApplication.class
})
public class KotlinModule {

    @Provides
    public NumberGenerator provideNumberGenerator() {
        return new RandomNumberGenerator();
    }

    @Provides
    public NameGenerator provideNameGenerator(NumberGenerator numberGenerator) {
        return new KotlinNameGenerator(numberGenerator);
    }

    @Provides
    public DrawActivityPresenter provideDrawActivityPresenter(NameGenerator nameGenerator) {
        return new DrawActivityPresenter(nameGenerator);
    }

//    @Provides
//    internal fun provideNameGenerator(numberGenerator: NumberGenerator): NameGenerator {
//        return KotlinNameGenerator(numberGenerator)
//    }
}
