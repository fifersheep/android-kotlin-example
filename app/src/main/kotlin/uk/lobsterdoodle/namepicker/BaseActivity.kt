package uk.lobsterdoodle.namepicker

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import butterknife.ButterKnife
import uk.lobsterdoodle.namepicker.annotation.ContentView
import uk.lobsterdoodle.namepicker.app.NamesApplication

open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        NamesApplication.get(this).injectDependencies(this)
        ButterKnife.inject(this)
//        setContentView(javaClass.getAnnotation(ContentView::class.java).value())
    }
}
