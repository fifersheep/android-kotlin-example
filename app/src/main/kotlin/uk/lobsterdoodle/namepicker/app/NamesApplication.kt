package uk.lobsterdoodle.namepicker.app

import android.app.Application
import android.content.Context
import dagger.ObjectGraph
import uk.lobsterdoodle.namepicker.dependency.KotlinModule

class NamesApplication : Application() {
    private val objectGraph: ObjectGraph = ObjectGraph.create(KotlinModule())

    companion object {
        fun get(context: Context): NamesApplication {
            return context.applicationContext as NamesApplication
        }
    }

    override fun onCreate() {
        super.onCreate()
        objectGraph.inject(this)
    }

    fun injectDependencies(obj: Any) {
        objectGraph.inject(obj)
    }
}