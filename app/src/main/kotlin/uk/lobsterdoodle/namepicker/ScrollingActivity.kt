package uk.lobsterdoodle.namepicker

import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import uk.lobsterdoodle.namepicker.presenter.DrawActivityPresenter
import uk.lobsterdoodle.namepicker.view.DrawActivityView
import javax.inject.Inject

class ScrollingActivity : BaseActivity(), DrawActivityView {

    @Inject
    lateinit var presenter: DrawActivityPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scrolling)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        presenter.onCreate(this)

        val fab = findViewById(R.id.fab) as FloatingActionButton
        fab.setOnClickListener { view -> presenter.drawName(arrayOf("Scott", "Myrto", "Rory", "Anders", "Peter")) }
    }

    override fun showName(name: String) {
        val toolbar = findViewById(R.id.toolbar_layout) as CollapsingToolbarLayout
        toolbar.title = name
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_scrolling, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return item.itemId == R.id.action_settings || super.onOptionsItemSelected(item)
    }
}
