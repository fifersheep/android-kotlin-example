package uk.lobsterdoodle.namepicker.presenter

import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import uk.lobsterdoodle.namepicker.core.NameGenerator
import uk.lobsterdoodle.namepicker.view.DrawActivityView

class DrawActivityPresenterTest {
    private lateinit var presenter: DrawActivityPresenter
    private lateinit var generator: NameGenerator
    private lateinit var view: DrawActivityView

    @Before
    fun setUp() {
        generator = mock(NameGenerator::class.java)
        view = mock(DrawActivityView::class.java)
        presenter = DrawActivityPresenter(generator)
        presenter.onCreate(view)
    }

    @Test
    fun returns_generated_name_from_list_of_names() {
        val names = arrayOf("Adam", "Brian", "Charlie", "David", "Edward", "Fiona")
        `when`(generator.pickFrom(names)).thenReturn("David")
        presenter.drawName(names)
        verify(view).showName("David")
    }
}