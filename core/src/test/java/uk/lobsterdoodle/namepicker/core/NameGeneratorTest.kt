package uk.lobsterdoodle.namepicker.core

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

class NameGeneratorTest {
    private lateinit var nameGenerator: KotlinNameGenerator
    private lateinit var numberGenerator: NumberGenerator

    @Before
    fun setUp() {
        numberGenerator = mock(NumberGenerator::class.java)
        nameGenerator = KotlinNameGenerator(numberGenerator)
    }

    @Test
    fun returns_name_in_position_of_generated_number() {
        val names = arrayOf("Adam", "Brian", "Charlie", "David", "Edward", "Fiona")
        `when`(numberGenerator.generate(names.size)).thenReturn(2)
        assertEquals(nameGenerator.pickFrom(names), "Charlie")
    }
}