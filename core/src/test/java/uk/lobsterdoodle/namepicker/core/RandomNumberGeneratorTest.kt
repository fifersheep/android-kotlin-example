package uk.lobsterdoodle.namepicker.core

import org.junit.Assert.*
import org.junit.Test

class RandomNumberGeneratorTest {

    val gen = RandomNumberGenerator()

    @Test
    fun generates_numbers_within_range() {
        var expectedNumbers = listOf(0, 1, 2)

        (0..50).forEach {
            assertTrue(expectedNumbers.contains(gen.generate(3)))
        }
    }
}