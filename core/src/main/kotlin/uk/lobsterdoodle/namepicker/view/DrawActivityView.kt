package uk.lobsterdoodle.namepicker.view

interface DrawActivityView {
    fun showName(name: String)
}