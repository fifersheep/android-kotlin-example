package uk.lobsterdoodle.namepicker.presenter

import uk.lobsterdoodle.namepicker.core.NameGenerator
import uk.lobsterdoodle.namepicker.view.DrawActivityView

class DrawActivityPresenter(val generator: NameGenerator) {
    private lateinit var view: DrawActivityView

    fun onCreate(view: DrawActivityView) {
        this.view = view
    }

    fun drawName(names: Array<String>) {
        val name = generator.pickFrom(names)
        view.showName(name)
    }
}
