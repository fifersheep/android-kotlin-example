package uk.lobsterdoodle.namepicker.annotation

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ContentView(val resId: Int)

//@ContentView(resId = 1) class C

//@Qualifier
//@Retention(AnnotationRetention.RUNTIME)
//annotation class ForApplication