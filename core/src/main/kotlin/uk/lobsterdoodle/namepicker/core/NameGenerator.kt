package uk.lobsterdoodle.namepicker.core

interface NameGenerator {
    fun pickFrom(names: Array<String>) : String
}