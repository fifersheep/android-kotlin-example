package uk.lobsterdoodle.namepicker.core

import uk.lobsterdoodle.namepicker.injection.MakeMeInjectable
import javax.inject.Inject

class KotlinNameGenerator(val numberGenerator: NumberGenerator) : NameGenerator {

    @field:Inject
    lateinit var m : MakeMeInjectable

    override fun pickFrom(names: Array<String>) : String {
        return names[numberGenerator.generate(names.size)]
    }
}
