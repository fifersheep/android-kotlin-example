package uk.lobsterdoodle.namepicker.core

interface NumberGenerator {
    fun generate(max: Int): Int
}