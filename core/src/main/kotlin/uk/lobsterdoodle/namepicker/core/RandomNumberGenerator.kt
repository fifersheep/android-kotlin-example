package uk.lobsterdoodle.namepicker.core

import java.util.Random

class RandomNumberGenerator : NumberGenerator {

    override fun generate(max: Int): Int {
        return Random().nextInt(max)
    }
}